import { IonContent, IonHeader, IonIcon, IonItem, IonPage , IonText, IonTitle, IonToolbar} from '@ionic/react'
import React, {useState} from 'react'
import {sunnyOutline as sunny, moonOutline as moon} from "ionicons/icons"
import "./Page.css"


interface Props {
    title?:string;
    noHeader?:boolean
}
const Pages:React.FC<Props> = ({title,children, noHeader}) => {
const [darkTheme, setDarkTheme] = useState<boolean>(false)

const handleClick = () =>{
document.body.classList.toggle("dark")
setDarkTheme(!darkTheme)
}

return (
    <IonPage >
                {/* <IonToolbar>
                <IonItem 
                slot='end' 
                color='trasparent' 
                lines='none'
                 onClick={handleClick}>
                   <IonIcon icon={darkTheme ? sunny : moon}></IonIcon> 
                </IonItem>
                <IonTitle>{title}</IonTitle>
                </IonToolbar> */}
                {
                    !noHeader &&
                    <IonHeader translucent mode='ios'>
                    <IonToolbar>
                     <IonItem 
                slot='end' 
                color='trasparent' 
                lines='none'
                 onClick={handleClick}>
                   <IonIcon icon={darkTheme ? sunny : moon}></IonIcon> 
                </IonItem>
                <IonTitle>{title}</IonTitle>
                </IonToolbar>
                </IonHeader>
                }
           
        <IonContent fullscreen>
            
            {noHeader && 
            <div>
                <IonText class='title' >{title} </IonText>
                {/* <IonIcon icon={"../assets/icon/atom-solid.svg"}  />  */}
            </div>}
            {children}
        </IonContent>
    </IonPage>
  )
}

export default Pages