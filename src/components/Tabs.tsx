import React from 'react';
import { IonTabs, IonTabBar, IonTabButton, IonIcon, IonLabel, IonText} from '@ionic/react';
import {    person  } from 'ionicons/icons';


export const TabsExample: React.FC = ({children}) => (
  <IonTabs>
      {children}
    <IonTabBar slot="bottom">
      <IonTabButton tab="tabla" href='/tabla' >
        <IonIcon icon={"../assets/icon/atom-solid.svg"}/> 
        <IonText>Elementos</IonText>
      </IonTabButton> 

      <IonTabButton tab='personal' href="/personal">
        <IonIcon icon={person} />
        <IonLabel>contacto</IonLabel>
      </IonTabButton>

  
    </IonTabBar>
  </IonTabs>
);

export { TabsExample as Tabs}