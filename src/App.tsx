import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet, setupIonicReact } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';


/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import { Tabs } from './components/Tabs';
import Tabla from './pages/Tabla';
import { Personal } from './pages/Personal';
import Element from './pages/Element';

setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <Tabs>
      <IonRouterOutlet>
        <Route exact path="/tabla">
         <Tabla/>
        </Route>
        <Route exact path="/element/:number/:classElement">
         <Element/>
        </Route>
        <Route exact path="/personal">
         <Personal/>
        </Route>
        <Route exact path="/">
          <Redirect to="/tabla" />
        </Route>
      </IonRouterOutlet>
      </Tabs>
    </IonReactRouter>
  </IonApp>
);

export default App;
