import {  IonList, IonListHeader,  IonTitle, IonIcon,IonText, IonItem, IonButton, IonGrid, IonRow, IonCol, IonImg, IonHeader, IonToolbar } from '@ionic/react'
import  { useEffect, useState } from 'react'
import { useParams,  } from 'react-router'

import Pages from '../components/Pages'
import elementos from "../elementos.json"
import {medical} from 'ionicons/icons'
import "./Element.css"
interface params{
    number:string,
    classElement:string
}
interface element{
    name:string|null;
    appearance:string|null;
    atomic_mass:number|null;
    boil:number|null;
    category:string|null
    color:null|string;
    density:number|null;
    discovered_by:string|null;
    melt:number|null;
    molar_heat:number|null;
    named_by:string|null;
    number:number|null;
    period:number|null;
    phase:string|null;
    source:string|undefined;
    spectral_img:string|null;
    summary:string|null;
    symbol:string|null;
    xpos:number|null;
    ypos:number|null;
    shells:number[];
    electron_configuration:string|null;
    electron_configuration_semantic:string|null;
    electron_affinity:number|null;
    electronegativity_pauling:number|null;
    ionization_energies:number[];
    "cpk-hex":string|null;


}

const Element = () => {
    let {number, classElement} = useParams<params>()
    const [tablaElement, setTablaElement] = useState<element>()
    useEffect(() => {
      const {elements} = elementos
       const datos = elements.find(item=>item.number===parseInt(number))
        setTablaElement(datos)
      return () => {
        console.log("abandono la pagina")
      }
    }, [number])
    
  return (
    <Pages noHeader>
      <IonHeader>
        <IonToolbar>
        <IonIcon icon={"../assets/icon/flask-solid.svg"} className={`icon ${classElement}`} /> 
            <IonText 
            className='element__title'>
              {tablaElement?.name}</IonText>
        </IonToolbar>
      </IonHeader>
        <IonList>         
          <IonItem lines='none'>
            <IonList>
              <IonListHeader className='item_header'>
                  <IonText className='item_title'>Dates</IonText>
              </IonListHeader>
              <IonItem lines='none'>
                <IonGrid>
                <IonImg 
      src={"../assets/icon/science.svg"} 
      alt='background'
      className='page__img'></IonImg>
                  <IonRow>
                    <IonCol>
                      <IonIcon 
                      icon={medical} 
                      className={classElement}
                      inlist
                      ></IonIcon>
                      <IonText>Apperece : </IonText>
                    </IonCol>
                    <IonCol>
                      <IonText>{tablaElement?.appearance}</IonText>
                    </IonCol>
                  </IonRow>
                  <IonRow>
                    <IonCol>
                    <IonIcon 
                      icon={medical} 
                      className={classElement}
                      inlist
                      ></IonIcon>
                      <IonText>Category : </IonText>
                    </IonCol>
                    <IonCol>
                      <IonText>{tablaElement?.category}</IonText>
                    </IonCol>
                  </IonRow>
                  <IonRow>
                    <IonCol>
                    <IonIcon 
                      icon={medical} 
                      className={classElement}
                      inlist
                      ></IonIcon>
                      <IonText>Discovery By:</IonText>
                    </IonCol>
                    <IonCol>
                      <IonText>
                        {tablaElement?.discovered_by}
                      </IonText>
                    </IonCol>
                  </IonRow>
                  <IonRow>
                    <IonCol>
                    <IonIcon 
                      icon={medical} 
                      className={classElement}
                      inlist
                      ></IonIcon>
                      <IonText>Boil : </IonText>
                    </IonCol>
                    <IonCol>
                      <IonText>
                        {tablaElement?.boil}
                      </IonText>
                    </IonCol>
                  </IonRow>
                  <IonRow>
                    <IonCol>
                    <IonIcon 
                      icon={medical} 
                      className={classElement}
                      inlist
                      ></IonIcon>
                      <IonText>Density : </IonText>
                    </IonCol>
                    <IonCol>
                      <IonText>
                        {tablaElement?.density}
                      </IonText>
                    </IonCol>
                  </IonRow>
                  <IonRow>
                    <IonCol>
                    <IonIcon 
                      icon={medical} 
                      className={classElement}
                      inlist
                      ></IonIcon>
                      <IonText>Confi </IonText>
                    </IonCol>
                    <IonCol>
                      <IonText>
                        {tablaElement?.electron_configuration}
                      </IonText>
                    </IonCol>
                  </IonRow>

                </IonGrid>
              
               
                
              </IonItem>
            </IonList>
          </IonItem>
          <IonItem lines='none'>
            <IonList>
              <IonListHeader 
              className='item_header'>
                  <IonText className='item_title'>Sumary</IonText>
              </IonListHeader>
              <IonItem lines='none'> 
                <IonText>
                  {tablaElement?.summary}
                </IonText>
              </IonItem>
            </IonList>
          </IonItem>
          <IonItem lines='none'>
            <IonButton 
            shape='round' 
            fill='outline' 
            color='success'
            href={tablaElement?.source}
            >more information</IonButton>
          </IonItem>
        </IonList>
    </Pages>
  )
}

export default Element