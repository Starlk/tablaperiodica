import { IonCard, IonGrid, IonRow, IonCol, IonCardHeader, IonLabel, IonCardContent, IonCardTitle, IonCardSubtitle, IonItem, IonText, IonSelect,IonSelectOption } from '@ionic/react'
import React,{useState} from 'react'
import Pages from '../components/Pages'
import elementos from "../elementos.json"
import "./Tabla.css"

const ColorOfElement:any = {
  "alkaline earth metal":"orage",
  "alkali metal":"orageDark",
  "post-transition metal":"yellowDark",
  "transition metal":"yellow",
  "metalloid":"green",
  "polyatomic nonmetal":"blueDark",
  "diatomic nonmetal":"blueClear",
  "noble gas":"purpleDark",
  "lanthanide":"pinkClear",
  "actinide":"pinkDark",
  "unknown, probably transition metal":"brownClear",
  "unknown, probably post-transition metal":"brownDark",
  "unknown, probably metalloid":"greenDark"
}


const Tabla = () => {
 const {elements} = elementos
 const [tablaElement, setTablaElement] = useState(elements)
 const Categories = Object.keys(ColorOfElement).sort()



const handleCategory = (category:string) : void =>{
    if(category==="All") setTablaElement(elements)
    else setTablaElement(elements.filter(el=>el.category===category))
}

  return (
    <Pages title='Tabla periodica' noHeader>
      <IonGrid>
        <IonRow>
          <IonCol size='12'  >
            <IonItem>
              <IonSelect 
              placeholder='select category' 
              okText="Okay" 
              cancelText="Cancel" 
              mode='ios' 
              className='select'
              onIonChange={(e)=>handleCategory(e.detail.value)}
              >
                <IonSelectOption value="All">All elements</IonSelectOption>
                 {
                   Categories.map((category,key)=>(
                     <IonSelectOption value={category} key={`${category}=${key}`} className="select__option">{category}</IonSelectOption>
                   ))
                 }
                </IonSelect>
            </IonItem>
          </IonCol>
        </IonRow>
        <IonRow className='ion-align-items-center '>    
        {tablaElement.map((el,idx)=>{
         
          return (
          <IonCol key={idx} size="6"  sizeMd='6' sizeLg='3'  sizeSm='6' className="ion-align-self-center "> 
            <div>
            <IonCard 
            className={`${ColorOfElement[el.category]} card`} 
            href={`/element/${el.number}/${ColorOfElement[el.category]}`}  
            routerDirection="root"
            >
              <IonCardHeader className='header'>
                <IonText className='number_atom'>{el.number}</IonText>
                <IonLabel class='item'>{el.atomic_mass.toFixed(2)}</IonLabel>
              </IonCardHeader>
              <IonCardContent>
                <IonCardTitle className="symbol">{el.symbol}</IonCardTitle>
                <IonCardSubtitle className='name'>
                  {el.name}
                  </IonCardSubtitle>
                <IonItem class='item'>{el.atomic_mass.toFixed(2)}</IonItem>
              </IonCardContent>
            </IonCard>
            </div>
          </IonCol>
        )})}
        </IonRow>
  </IonGrid>
    </Pages>
  )
}

export default Tabla