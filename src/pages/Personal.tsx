import { IonImg, IonItem, IonItemDivider, IonList, IonListHeader, IonText } from '@ionic/react'
import React from 'react'
import Pages from '../components/Pages'

export const Personal = () => {
  return (
    <Pages title='personal'  noHeader>
       <IonList mode='ios'>
         <IonListHeader>
           <IonImg alt='perfil' src={"../assets/Img/perfil.jpg"}
           className="img"
           ></IonImg>
         </IonListHeader>
         <IonItemDivider>Nombre</IonItemDivider>
         <IonItem>
            <IonText>Starlin Dariel de Jesus Medina</IonText>
         </IonItem>
         <IonItemDivider>Correo</IonItemDivider>
         <IonItem>
            <IonText>starlinjesus612@gmail.com</IonText>
         </IonItem>
         <IonItemDivider>Telefono</IonItemDivider>
         <IonItem>
            <IonText>+1 (829)-341-9917</IonText>
         </IonItem>
       </IonList>
    </Pages>
  )
}
