import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'tablaPeriodica',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
